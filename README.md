Project 5
Author:Otis Greer
Email: ogreer@uoregon.edu
Description: This program will use MongoDb alongside the docker-compose file and the code from the previous project in order to take in the distances given by the user to generate times for opening and closing brevet gates. When the times are entered, hitting the 'Submit' button will place the times into a database. When the 'Display' button is clicked, the user will be taken to a page where the times and distances will be displayed. If there is data inside the databasae, they will remain until they are cleared out by new data.
