"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    date = request.args.get('begin_date')
    time = request.args.get('begin_time')
    distance = request.args.get('distance')   
    date_time = (date +" " +time)

    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance,date_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/submit')
def submit():
    data = request.args.get('res')
    print(data == "")
    if data == "":
        res = "False"
    else:
        res = "True"
        db.caldb.drop()
        db.distance.drop()
        distance = request.args.get('distance', 999, type=int)
        distances_list = {}
        distances_list['distance'] = distance
        db.distance.insert_one(distances_list)
        new_array = data.split(',')
        array_length = len(new_array)
        pair_num = int(array_length/4)
        counter = 0
        for index in range(pair_num):
            distance_dic = {}
            distance_dic['mile'] = new_array[counter]
            counter += 1
            distance_dic['km'] = new_array[counter]
            counter += 1
            distance_dic['opentime'] = new_array[counter]
            counter += 1
            distance_dic['closetime'] = new_array[counter]
            counter += 1
            db.caldb.insert_one(distance_dic)
    result = {"res": res}
    return flask.jsonify(result=result)


"""
This will take the found open and close time data from this python file when times are entered into the new show_times.html page to show the times
"""
@app.route("/results")
def results():
    _items = db.caldb.find()
    items = [item for item in _items]
    found_distances = db.distance.find()
    distance = [distance for distance in found_distances]
    print(distance, " inside the display")
    return render_template('show_times.html', items=items, distance=distance)


@app.route('/display')
def display():
    if db.caldb.count() == 0:
        res = "False"
    else:
        res = "True"
    result = {"res": res}
    return flask.jsonify(result=result)
#############


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)



if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
